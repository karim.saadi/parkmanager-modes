import { ApiProperty } from '@nestjs/swagger';
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, DeleteDateColumn } from 'typeorm';

@Entity()
export class ParkingPlace {
    @PrimaryGeneratedColumn()
    @ApiProperty()
    id: number;

    @Column({ nullable: false })
    @ApiProperty()
    number: string;

    @Column({ nullable: false })
    @ApiProperty()
    floor: string;

    @Column({
        default: true,
        nullable: false
    })
    @ApiProperty()
    isActive: boolean;

    @CreateDateColumn()
    @ApiProperty()
    createdAt: Date;

    @UpdateDateColumn()
    @ApiProperty()
    updatedAt: Date;

    @DeleteDateColumn()
    @ApiProperty()
    deletedAt: Date;
}