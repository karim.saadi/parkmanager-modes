import { ApiProperty } from '@nestjs/swagger';
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, DeleteDateColumn } from 'typeorm';

@Entity()
export class Auth {
    @PrimaryGeneratedColumn('increment')
    @ApiProperty()
    id: number;

    @Column({
        unique: true,
        nullable: false
    })
    @ApiProperty()
    userId: number;

    @Column({
        unique: true,
        nullable: false
    })
    @ApiProperty()
    email: string;

    @Column({ nullable: false })
    @ApiProperty()
    password: string;

    @Column({
        nullable: true,
        default: null
    })
    @ApiProperty()
    lastLogin: Date;

    @CreateDateColumn()
    @ApiProperty()
    createdAt: Date;

    @UpdateDateColumn()
    @ApiProperty()
    updatedAt: Date;

    @DeleteDateColumn()
    @ApiProperty()
    deletedAt: Date;
}