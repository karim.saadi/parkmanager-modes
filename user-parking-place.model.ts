import { ApiProperty } from '@nestjs/swagger';
import { ParkingPlace } from 'parking-place.model';
import { Entity, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, PrimaryColumn } from 'typeorm';

@Entity()
export class UserParkingPlace {
    @PrimaryColumn('int')
    userId: number;

    @PrimaryColumn('int')
    parkingPlaceId: number;

    parkingPlace: ParkingPlace;

    @Column({
        nullable: false
    })
    @ApiProperty()
    start: Date;

    @Column({
        nullable: false
    })
    @ApiProperty()
    end: Date;

    @CreateDateColumn()
    @ApiProperty()
    createddAt: Date;

    @UpdateDateColumn()
    @ApiProperty()
    updatedAt: Date;

    @DeleteDateColumn()
    @ApiProperty()
    deletedAt: Date;
}