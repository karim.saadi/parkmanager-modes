import { ApiProperty } from '@nestjs/swagger';
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, DeleteDateColumn } from 'typeorm';
@Entity()
export class User {
    @PrimaryGeneratedColumn('increment')
    @ApiProperty()
    id: number;

    @Column({
        unique: true,
        nullable: false
    })
    @ApiProperty()
    username: string;

    @Column({
        default: true,
        nullable: false
    })
    @ApiProperty()
    isActive: boolean;

    @Column({
        default: false
    })
    @ApiProperty()
    isAdmin: boolean;

    @CreateDateColumn()
    @ApiProperty()
    createdAt: Date

    @UpdateDateColumn()
    @ApiProperty()
    updatedAt: Date

    @DeleteDateColumn()
    @ApiProperty()
    deletedAt: Date
}